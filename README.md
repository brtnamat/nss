# PITCHER, Systém Rezervace Sportovišť

<a href="https://docs.google.com/document/d/1CLJ-p2eQh36Sya04RW-5EdbNPJvHOxJf0Jy5swI5CD4/edit?usp=sharing" target="_blank">Odkaz na dokument</a>
## Přehled

Tento projekt je systém rezervace sportovišť postavený v Java a Spring Boot. Zahrnuje backendovou API pro správu rezervací, autentifikaci uživatelů a další funkcionality. Projekt využívá různé architektonické a design patterny a několik technologií k zajištění robustního řešení.

## Použité Technologie

- **Java 17**: Projekt je postaven na Java 17, což zajišťuje kompatibilitu s moderními funkcemi a vylepšeními.
- **Spring Boot**: Poskytuje framework pro budování RESTful API a správu konfigurace aplikace.
- **PostgreSQL**: Relace databáze používaná pro ukládání dat aplikace.
- **Hazelcast**: Je v tomto projektu použit na cachování určitých dat z API endpointů. 
- **Docker**: Používá se pro kontejnerizaci aplikace a jejích závislostí.
- **Docker Compose**: Spravuje multi-kontejnerové Docker aplikace, včetně služeb jako backend, databáze a další.
- **Interceptor**: Zachytává příchozí požadavky a jejich zpracování, které následně zapisuje do souboru "interceptor.log"


## Nepoužité Technologie

- **Kafka**: Kafka v našem projektu nebyla použita.
- **Elastic search**: Elastic search jsme původně použít chtěli, ale nakonec se to nepovedlo.

## Implementované Design Patterny

1. **Singleton Pattern**: Třída `JwtService` je navržena jako singleton, aby zajistila používání jediného instance ve všech částech aplikace.

2. **Factory Pattern**: `FieldController` používá tovární metody pro vytváření instancí filtrů, které se používají k validaci rezervací.

3. **Strategy Pattern**: Filtry ve třídě `ReservationPipeline` fungují jako strategie aplikované na objekty rezervací.

4. **Decorator Pattern**: Třída `CustomGrantedAuthority` implementuje `GrantedAuthority`, která přidává dodatečné chování k rolím uživatelů.

5. **Template Method Pattern**: Třída `ReservationPipeline` používá šablonovou metodu (`process()`) pro zpracování rezervací prostřednictvím sekvence filtrů.

## Jak Začít

### Požadavky

- Docker
- Docker Compose

### Spuštění Aplikace

1. **Klonování Repozitáře**

```
git clone https://gitlab.fel.cvut.cz/brtnamat/nss.git && cd nss

```

2. **Klonování Repozitáře**

Jako další je potřeba spustit ```mvn clean package``` aby se udělal v ```/target``` složce ```.jar``` soubor, z kterého se postaví backend container.

3. **Sestavení a Start Kontejnerů**
```
sudo docker compose build
docker compose up
```

To spustí následující služby:
- **frontend**: Frontendová služba běžící na portu 8080.
- **backend**: Backendová služba běžící na portu 8081.
- **db**: PostgreSQL databáze běžící na portu 5432.

3. **Přístup k Aplikaci**

- **Frontend**: [http://localhost:8080](http://localhost:8080)
- **Backend**: [http://localhost:8081](http://localhost:8081)

- naplní se databáze základními uživateli, hřišti, sport centry a nějakými rezervacemi od uživatelů.

    |  Role      |  Username       | Password   |
    |------------|-----------------|------------|
    |  **Owner** |  janesmithowner | Heslo123   |
    |  **Player**|  johndoeplayer  | Heslo123 |
    | **Admin**  | johndoe         | Heslo123   |


4. **Zastavení Kontejnerů**
```
docker compose down
```

## API Endpoints

API endpointy jsou ve všech třídách v package controller, níže příklad.

### Field Controller

- `GET /api/fields/getReservations/{field_id}`: Získá kalendář rezervací pro dané sportoviště.

### Autentifikace

- JWT autentifikace se používá k zabezpečení API endpointů. Třída `JwtAuthFilter` zpracovává autentifikaci prostřednictvím JWT tokenů.

## Design Patterny

- **Singleton Pattern**: Použit v `JwtService`.
- **Factory Pattern**: Použit pro vytváření instancí filtrů v `FieldController`.
- **Strategy Pattern**: Filtry v `ReservationPipeline` fungují jako strategie.
- **Decorator Pattern**: Použit v `CustomGrantedAuthority`.
- **Template Method Pattern**: Použit v třídě `ReservationPipeline`.

## Nasazení

Pro nasazení na produkční server můžete použít platformy jako Heroku nebo jiné cloudové služby. Ujistěte se, že jsou správně nastavené proměnné prostředí a konfigurace. My jsme se snažili o nasazení na Google Cloud Platform, ale bylo to až moc probrčených nocí :Dd

## Licence

Tento projekt je licencován pod licencí MIT