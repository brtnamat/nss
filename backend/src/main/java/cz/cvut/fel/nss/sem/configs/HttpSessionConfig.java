package cz.cvut.fel.nss.sem.configs;

import com.hazelcast.core.HazelcastInstance;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.session.config.annotation.web.http.EnableSpringHttpSession;
import org.springframework.session.hazelcast.HazelcastIndexedSessionRepository;
import org.springframework.session.web.http.CookieHttpSessionIdResolver;
import org.springframework.session.web.http.HttpSessionIdResolver;

@Configuration
@EnableSpringHttpSession
public class HttpSessionConfig {

    @Bean
    public HazelcastIndexedSessionRepository hazelcastSessionRepository(HazelcastInstance hazelcastInstance) {
        return new HazelcastIndexedSessionRepository(hazelcastInstance);
    }

    @Bean
    public HttpSessionIdResolver httpSessionIdResolver() {
        return new CookieHttpSessionIdResolver();
    }
}
