package cz.cvut.fel.nss.sem.services;

import cz.cvut.fel.nss.sem.DTO.ReservationDTO;
import cz.cvut.fel.nss.sem.entities.payments.Payment;
import cz.cvut.fel.nss.sem.entities.reservation.Reservation;
import cz.cvut.fel.nss.sem.entities.users.Player;
import cz.cvut.fel.nss.sem.filters.*;
import cz.cvut.fel.nss.sem.pipes.Pipe;
import cz.cvut.fel.nss.sem.repository.FieldRepository;
import cz.cvut.fel.nss.sem.repository.PlayerRepository;
import cz.cvut.fel.nss.sem.repository.ReservationRepository;
import org.apache.kafka.common.errors.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@EnableCaching
public class ReservationService {

    private static final Logger logger = LoggerFactory.getLogger(ReservationService.class);

    private final ReservationRepository reservationRepository;
    private final PlayerRepository playerRepository;
    private final FieldRepository fieldRepository;

    @Autowired
    public ReservationService(ReservationRepository reservationRepository, PlayerRepository playerRepository, FieldRepository fieldRepository) {
        this.reservationRepository = reservationRepository;
        this.playerRepository = playerRepository;
        this.fieldRepository = fieldRepository;
    }

    @Cacheable(value = "reservation", key = "#reservationId")
    public ReservationDTO.Detailed getReservation(Long reservationId) {
        Reservation reservation = reservationRepository.findById(reservationId)
                .orElseThrow(() -> new ResourceNotFoundException("Reservation not found"));
        return new ReservationDTO.Detailed(reservation);
    }

    @Cacheable("Reservations")
    public List<ReservationDTO.Basic> fetchReservations() {
        List<Reservation> reservations = reservationRepository.findAll();
        return reservations.stream()
                .map(ReservationDTO.Basic::new)
                .collect(Collectors.toList());
    }

    public String checkReservationAvailability(ReservationDTO.Creating input) throws Exception {
        logger.info("Check availability runned");

        List<Filter<Reservation>> filters = Arrays.asList(
                new UserValidationFilter(playerRepository),
                new FieldValidationFilter(fieldRepository),
                new DateValidationFilter(),
                new AvailabilityFilter(reservationRepository)
        );

        Reservation reservation = generateReservationFromInput(input);

        Pipe<Reservation> pipe = new Pipe<>(filters, reservation);

        logger.info("Executing filters");
        if (pipe.fire()) {
            return "Reservation is available. Proceed to payment.";
        }
        return "Reservation is not available.";
    }

    @CacheEvict(value = "Reservations", allEntries = true)
    public ReservationDTO.Basic confirmReservation(ReservationDTO.Creating input) throws Exception {

        checkReservationAvailability(input);
        Reservation reservation = generateReservationFromInput(input);

        PaymentFilter paymentFilter = new PaymentFilter(playerRepository);
        paymentFilter.execute(reservation);

        Player player = reservation.getReservatingPlayer();
        player.addReservation(reservation);
        logger.info(player.toString());

        reservation.getReservatedField().addReservation(reservation);
        Reservation savedReservation = reservationRepository.save(reservation);
        playerRepository.save(player);

        return new ReservationDTO.Basic(savedReservation);
    }

    @CacheEvict(value = "reservationCalendars", allEntries = true)
    public Reservation generateReservationFromInput(ReservationDTO.Creating input) {
        return new Reservation(
                playerRepository.findByPlayerId(input.getPlayerId()),
                fieldRepository.findByFieldId(input.getFieldId()),
                new Payment(),
                input.getStartDateHour(),
                input.getEndDateHour()
        );
    }
}