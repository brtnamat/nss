package cz.cvut.fel.nss.sem.entities;

import jakarta.persistence.*;
import lombok.*;


@Getter
@Setter
@Embeddable
public class BankAccount {
    Integer number;
    Integer codeOfBank;
}
