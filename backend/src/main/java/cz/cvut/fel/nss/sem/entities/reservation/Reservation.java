package cz.cvut.fel.nss.sem.entities.reservation;

import cz.cvut.fel.nss.sem.entities.users.*;
import cz.cvut.fel.nss.sem.entities.payments.*;
import cz.cvut.fel.nss.sem.entities.Fields.*;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.*;

import java.time.Duration;
import java.time.LocalDateTime;

@Entity
@AllArgsConstructor
@Builder
@Getter
@Setter
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long reservationId;

    @ManyToOne
    @JoinColumn(name = "userId")
    Player reservatingPlayer;

    @ManyToOne
    @JoinColumn(name = "fieldId")
    Field reservatedField;

    @Embedded
    Payment payment;

    LocalDateTime startDateHour;

    LocalDateTime endDateHour;

    Long totalPrice;


    public Reservation(Player player, Field reservatedField, Payment payment, LocalDateTime startDateHour, LocalDateTime endDateHour) {
        this.reservatingPlayer = player;
        this.reservatedField = reservatedField;
        this.payment = payment;
        this.startDateHour = startDateHour;
        this.endDateHour = endDateHour;
        this.totalPrice = reservatedField.getHourRate() == null ? null : Duration.between(this.startDateHour, this.endDateHour).toHours() * reservatedField.getHourRate();
    }

    public Reservation() {

    }


}
