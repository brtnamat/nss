package cz.cvut.fel.nss.sem.DTO;
import cz.cvut.fel.nss.sem.entities.Address;
import cz.cvut.fel.nss.sem.entities.Fields.SportCenter;
import cz.cvut.fel.nss.sem.entities.payments.Payment;
import cz.cvut.fel.nss.sem.entities.payments.PaymentType;
import cz.cvut.fel.nss.sem.entities.reservation.Reservation;
import cz.cvut.fel.nss.sem.repository.FieldRepository;
import cz.cvut.fel.nss.sem.repository.PlayerRepository;
import cz.cvut.fel.nss.sem.repository.UserRepository;
import cz.cvut.fel.nss.sem.services.SportCenterService;
import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Duration;
import java.time.LocalDateTime;



@Data
public class ReservationDTO {


    @Data
    public static final class Basic {
        private Long reservationId;
        private Long playerId;
        private Long fieldId;
        private Address address;
        private LocalDateTime startDateHour;
        private LocalDateTime endDateHour;
        private Long totalPrice;


        public Basic(Reservation reservation){
            reservationId = reservation.getReservationId();
            playerId = reservation.getReservatingPlayer().getUserId();
            fieldId = reservation.getReservatedField().getFieldId();
            address = reservation.getReservatedField().getSportCenter().getAddress();
            startDateHour = reservation.getStartDateHour();
            endDateHour = reservation.getEndDateHour();
            totalPrice = reservation.getTotalPrice();
        }

    }

    @Data
    public static final class Detailed {
        private Long reservationId;
        private PlayerDTO.Basic reservatingPlayer;
        private FieldDTO.Basic reservatedField;
        private Address address;
        private PaymentType paymentType;
        private LocalDateTime startDateHour;
        private LocalDateTime endDateHour;
        private Payment payment;
        private Long totalPrice;

        public Detailed(Reservation reservation){
            reservationId = reservation.getReservationId();
            address = reservation.getReservatedField().getSportCenter().getAddress();
            reservatingPlayer = new PlayerDTO.Basic(reservation.getReservatingPlayer());
            reservatedField = new FieldDTO.Basic(reservation.getReservatedField());
            paymentType = reservation.getPayment().getPaymentType();
            payment = reservation.getPayment();
            startDateHour = reservation.getStartDateHour();
            endDateHour = reservation.getEndDateHour();
            totalPrice = reservation.getTotalPrice();
        }
    }

    @Data
    public static final class Creating {
        private Long playerId;
        private Long fieldId;
        private LocalDateTime startDateHour;
        private LocalDateTime endDateHour;

        public Creating(Long playerId, Long fieldId,LocalDateTime startDateHour, LocalDateTime endDateHour){
            this.playerId = playerId;
            this.fieldId = fieldId;
            this.startDateHour = startDateHour;
            this.endDateHour = endDateHour;
        }
    }

}