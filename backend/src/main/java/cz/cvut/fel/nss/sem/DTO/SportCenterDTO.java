package cz.cvut.fel.nss.sem.DTO;

import cz.cvut.fel.nss.sem.entities.Address;
import cz.cvut.fel.nss.sem.entities.Fields.SportCenter;
import lombok.*;

import java.util.List;
import java.util.stream.Collectors;

@Data
public class SportCenterDTO {

    @Data
    @AllArgsConstructor
    public static final class Basic {
        private String name;
        private Long sportCenterId;
        private Address address;
        private Long owner; // Assuming you only want owner's ID here
        private List<FieldDTO.Basic> fields; // Assuming Basic DTO for fields here

        public Basic(SportCenter sportCenter) {
            sportCenterId = sportCenter.getSportCenterId();
            address = sportCenter.getAddress();
            owner = sportCenter.getOwner().getUserId();
            fields = sportCenter.getFields().stream()
                    .map(FieldDTO.Basic::new)
                    .collect(Collectors.toList());
            name = sportCenter.getName();
        }
    }

    @Data
    public static final class Detailed {
        private String name;
        private Long sportCenterId;
        private Address address;
        private UserDTO.Basic owner;
        private List<FieldDTO.Detailed> fields;

        public Detailed(SportCenter sportCenter) {
            sportCenterId = sportCenter.getSportCenterId();
            address = sportCenter.getAddress();
            owner = new UserDTO.Basic(sportCenter.getOwner());
            fields = sportCenter.getFields().stream()
                    .map(FieldDTO.Detailed::new)
                    .collect(Collectors.toList());
            name=sportCenter.getName();
        }
    }
}