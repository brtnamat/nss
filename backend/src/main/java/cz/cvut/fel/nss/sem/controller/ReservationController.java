package cz.cvut.fel.nss.sem.controller;

import cz.cvut.fel.nss.sem.DTO.ReservationDTO;
import cz.cvut.fel.nss.sem.services.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@EnableCaching
@RestController
@RequestMapping("api/reservations")
public class ReservationController {

    private final ReservationService reservationService;

    @Autowired
    public ReservationController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @PreAuthorize("hasAuthority('ROLE_PLAYER') || hasAnyAuthority('ROLE_OWNER')")
    @CrossOrigin
    @GetMapping("/{reservationId}")
    public ResponseEntity<ReservationDTO.Detailed> getReservation(@PathVariable Long reservationId) {
        return ResponseEntity.ok(reservationService.getReservation(reservationId));
    }

    @CrossOrigin
    @GetMapping
    public ResponseEntity<List<ReservationDTO.Basic>> fetchReservations() {
        return ResponseEntity.ok(reservationService.fetchReservations());
    }

    @PreAuthorize("hasAuthority('ROLE_PLAYER')")
    @CrossOrigin
    @PostMapping("/check-availability")
    public ResponseEntity<String> checkReservationAvailability(@RequestBody ReservationDTO.Creating input) throws Exception {
        String result = reservationService.checkReservationAvailability(input);
        if (result.equals("Reservation is available. Proceed to payment.")) {
            return ResponseEntity.ok(result);
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(result);
    }

    @PreAuthorize("hasAuthority('ROLE_PLAYER')")
    @CrossOrigin
    @PostMapping("/confirm-reservation")
    public ResponseEntity<ReservationDTO.Basic> confirmReservation(@RequestBody ReservationDTO.Creating input) throws Exception {
        ReservationDTO.Basic result = reservationService.confirmReservation(input);
        return ResponseEntity.ok(result);
    }
}