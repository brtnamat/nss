package cz.cvut.fel.nss.sem.entities.users;
import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.cvut.fel.nss.sem.DTO.UserDTO;
import cz.cvut.fel.nss.sem.entities.*;
import cz.cvut.fel.nss.sem.entities.reservation.Reservation;
import cz.cvut.fel.nss.sem.utilities.CustomGrantedAuthority;
import jakarta.persistence.*;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;
import java.util.Collection;


//@MappedSuperclass
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "userType", discriminatorType = DiscriminatorType.STRING)
@Getter
@Setter
@Table(name = "users")

abstract public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="userId", unique = true, nullable = false)
    Long userId;

    @Column(columnDefinition = "VARCHAR(50)", name = "firstName")
    String firstName;

    @Column(columnDefinition = "VARCHAR(50)", name = "lastName")
    String lastName;
//    @JsonIgnore
    @Column(columnDefinition = "VARCHAR(100)", name = "username")
    String username;
//    @JsonIgnore
    @Column(columnDefinition = "VARCHAR(255)", name = "password")
    String password;
    @Column(columnDefinition = "VARCHAR(255)", name = "email")
    String mailAddress;
//    @JsonIgnore
    @Column(columnDefinition = "VARCHAR(9)", name = "phoneNumber")
    String phoneNumber;
//    @JsonIgnore
    @Column(columnDefinition = "VARCHAR(4)", name = "phoneCode")
    String phoneCode;
//    @JsonIgnore
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "street", column = @Column(name = "userStreet")),
            @AttributeOverride(name = "houseNumber", column = @Column(name = "userHouseNumber")),
            @AttributeOverride(name = "city", column = @Column(name = "userCity")),
            @AttributeOverride(name = "postalCode", column = @Column(name = "userPostalCode"))
    })
    private Address address;

//    @JsonIgnore
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "street", column = @Column(name = "billingStreet")),
            @AttributeOverride(name = "houseNumber", column = @Column(name = "billingHouseNumber")),
            @AttributeOverride(name = "city", column = @Column(name = "billingCity")),
            @AttributeOverride(name = "postalCode", column = @Column(name = "billingPostalCode"))
    })
    private Address billingAddress;

    public abstract Collection<CustomGrantedAuthority> getAuthorities();
}
