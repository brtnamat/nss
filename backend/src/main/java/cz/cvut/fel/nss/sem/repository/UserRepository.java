package cz.cvut.fel.nss.sem.repository;

import cz.cvut.fel.nss.sem.entities.users.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
        @Query("SELECT u FROM User u WHERE u.userId= :userId")
        User findByUserId(@Param("userId") Long userId);


        @Query("SELECT u FROM User u WHERE u.username= :username")
        Optional<User> findByUserUsername(@Param("username") String username);
//
//        @Query("SELECT a FROM Admin a WHERE a.user_id= :user_id")
//        User findByAdminId(@Param("user_id") Long user_id);



}
