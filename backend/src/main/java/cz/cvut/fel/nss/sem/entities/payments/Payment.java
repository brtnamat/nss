package cz.cvut.fel.nss.sem.entities.payments;

import cz.cvut.fel.nss.sem.entities.BankAccount;
import jakarta.persistence.Embeddable;
import jakarta.persistence.Embedded;
import lombok.*;


@Getter
@Setter
@Embeddable
public class Payment {
    @Embedded
    BankAccount bankAccount;

    PaymentType paymentType;
    Double amount;
    boolean paid;
}
