package cz.cvut.fel.nss.sem.DTO;

import cz.cvut.fel.nss.sem.entities.Fields.Field;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FieldDTO {
    private Long fieldId;
    private Integer width;
    private Integer length;
    private ReservationCalendarDTO reservationCalendar;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static final class Id {
        private Long fieldId;
    }

    @Data
    @AllArgsConstructor
    public static final class Basic {
        private Long fieldId;
        private Integer width;
        private Integer length;
        private Integer hourRate;

        public Basic(Field field) {
            this.fieldId = field.getFieldId();
            this.width = field.getWidth();
            this.length = field.getLength();
            this.hourRate = field.getHourRate();
        }
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static final class Detailed {
        private Long fieldId;
        private Integer width;
        private Integer length;
        private Integer hourRate;
        private ReservationCalendarDTO reservationCalendar;

        public Detailed(Field field) {
            this.fieldId = field.getFieldId();
            this.width = field.getWidth();
            this.length = field.getLength();
            this.reservationCalendar = new ReservationCalendarDTO(field.getReservationCalendar());
            this.hourRate = field.getHourRate();
        }
    }
}
