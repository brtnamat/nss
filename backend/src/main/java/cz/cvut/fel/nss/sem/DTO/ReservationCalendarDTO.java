package cz.cvut.fel.nss.sem.DTO;

import cz.cvut.fel.nss.sem.entities.reservation.Reservation;
import cz.cvut.fel.nss.sem.entities.reservation.ReservationCalendar;
import cz.cvut.fel.nss.sem.repository.FieldRepository;
import cz.cvut.fel.nss.sem.repository.PlayerRepository;
import cz.cvut.fel.nss.sem.repository.ReservationRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Data
public class ReservationCalendarDTO {

    private List<ReservationDTO.Basic> reservations;


    public ReservationCalendarDTO(ReservationCalendar calendar) {
        this.reservations = calendar.getReservations().stream()
                .map(ReservationDTO.Basic::new)
                .collect(Collectors.toList());
    }

//    public ReservationCalendar toReservationCalendar() {
//        ReservationCalendar calendar = new ReservationCalendar();
//
//        List<Reservation> reservationList = this.reservations.stream()
//                .map(reservationDTO -> {
//                    Reservation reservation = new Reservation();
//                    reservation.setReservationId(reservationDTO.getReservationId());
//                    reservation.setReservatingPlayer(reservationDTO.getPlayerId());
//                    reservation.setReservatedField(reservationDTO.getFieldId());
//                    reservation.setStartDateHour(reservationDTO.getStartDateHour());
//                    reservation.setEndDateHour(reservationDTO.getEndDateHour());
//                    return reservation;
//                })
//                .collect(Collectors.toList());
//
//        calendar.setReservations(reservationList);
//        return calendar;
//    }



}
