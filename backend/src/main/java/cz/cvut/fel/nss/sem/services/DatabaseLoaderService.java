package cz.cvut.fel.nss.sem.services;

import cz.cvut.fel.nss.sem.entities.Address;
import cz.cvut.fel.nss.sem.entities.Fields.Field;
import cz.cvut.fel.nss.sem.entities.Fields.SportCenter;
import cz.cvut.fel.nss.sem.entities.reservation.Reservation;
import cz.cvut.fel.nss.sem.entities.users.Admin;
import cz.cvut.fel.nss.sem.entities.users.Owner;
import cz.cvut.fel.nss.sem.entities.users.Player;
import cz.cvut.fel.nss.sem.repository.*;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
@Service
public class DatabaseLoaderService {

    @Autowired
    private final UserRepository userRepository;
    @Autowired
    private final SportCenterRepository sportCenterRepository;
    @Autowired
    private final FieldRepository fieldRepository;
    @Autowired
    private final ReservationRepository reservationRepository;

    PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    public DatabaseLoaderService( ReservationRepository reservationRepository, UserRepository userRepository, SportCenterRepository sportCenterRepository, FieldRepository fieldRepository) {
        this.sportCenterRepository = sportCenterRepository;
        this.fieldRepository = fieldRepository;
        this.reservationRepository = reservationRepository;
        this.userRepository = userRepository;
    }

    @PostConstruct
    public void loadData() {
        // Kontrola, zda už existuje owner s daným username
        if (userRepository.findByUserUsername("janesmithowner").isEmpty()) {
            Owner owner1 = Owner.builder()
                    .firstName("Jane")
                    .lastName("Smith")
                    .username("janesmithowner")
                    .password(passwordEncoder.encode("Heslo123"))
                    .mailAddress("jane.smith@example.com")
                    .phoneNumber("987654321")
                    .phoneCode("5678")
                    .address(Address.builder()
                            .street("Broadway")
                            .houseNumber("456")
                            .city("Los Angeles")
                            .postalCode("90001")
                            .build())
                    .build();
            userRepository.save(owner1);

            SportCenter sportCenter = SportCenter.builder()
                    .fields(new ArrayList<>())
                    .address(Address.builder()
                            .street("Main Street")
                            .houseNumber("123")
                            .city("New York")
                            .postalCode("10001")
                            .build())
                    .owner(owner1).build();

            sportCenterRepository.save(sportCenter);
            owner1.addSportCenter(sportCenter);
            userRepository.save(owner1);

            Field field1 = Field.builder().width(250).length(250).hourRate(250).sportCenter(sportCenter).build();
            Field field2 = Field.builder().width(200).length(220).hourRate(300).sportCenter(sportCenter).build();
            Field field3 = Field.builder().width(150).length(200).hourRate(350).sportCenter(sportCenter).build();

            fieldRepository.save(field1);
            fieldRepository.save(field2);
            fieldRepository.save(field3);
        }

        // Kontrola, zda už existuje admin s daným username
        if (userRepository.findByUserUsername("johndoe").isEmpty()) {
            Admin admin1 = Admin.builder()
                    .firstName("John")
                    .lastName("Doe")
                    .username("johndoe")
                    .password(passwordEncoder.encode("Heslo123"))
                    .mailAddress("john.doe@example.com")
                    .phoneNumber("123456789")
                    .phoneCode("1234")
                    .address(Address.builder()
                            .street("Main Street")
                            .houseNumber("123")
                            .city("New York")
                            .postalCode("10001")
                            .build())
                    .build();

            userRepository.save(admin1);
        }

        // Kontrola, zda už existuje player s daným username
        if (userRepository.findByUserUsername("johndoeplayer").isEmpty()) {
            Player player1 = Player.builder()
                    .firstName("John")
                    .lastName("Doe")
                    .username("johndoeplayer")
                    .password(passwordEncoder.encode("Heslo123"))
                    .mailAddress("john.doe@example.com")
                    .phoneNumber("123456789")
                    .phoneCode("1234")
                    .address(Address.builder()
                            .street("Main Street")
                            .houseNumber("123")
                            .city("New York")
                            .postalCode("10001")
                            .build())
                    .build();

            userRepository.save(player1);

            Field field1 = fieldRepository.findById(1L).orElseThrow(); // nebo najděte field podle jiného kritéria

            Reservation reservation1 = Reservation.builder()
                    .reservatingPlayer(player1)
                    .reservatedField(field1)
                    .startDateHour(LocalDateTime.of(2024, 9, 10, 10, 0))
                    .endDateHour(LocalDateTime.of(2024, 9, 10, 12, 0))
                    .build();

            Reservation reservation2 = Reservation.builder()
                    .reservatingPlayer(player1)
                    .reservatedField(field1)
                    .startDateHour(LocalDateTime.of(2024, 9, 12, 14, 0))
                    .endDateHour(LocalDateTime.of(2024, 9, 12, 16, 0))
                    .build();

            reservationRepository.save(reservation1);
            reservationRepository.save(reservation2);
        }
    }
}
