package cz.cvut.fel.nss.sem.entities.users;

import cz.cvut.fel.nss.sem.DTO.UserDTO;
import cz.cvut.fel.nss.sem.entities.*;
import cz.cvut.fel.nss.sem.entities.Fields.*;
import cz.cvut.fel.nss.sem.entities.reservation.Reservation;
import cz.cvut.fel.nss.sem.entities.reservation.ReservationCalendar;
import cz.cvut.fel.nss.sem.entities.payments.Payment;
import cz.cvut.fel.nss.sem.filters.AvailabilityFilter;
import cz.cvut.fel.nss.sem.utilities.CustomGrantedAuthority;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.Transient;
import lombok.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.security.core.GrantedAuthority;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@DiscriminatorValue("PLAYER")
@NoArgsConstructor
@Entity
@Getter
@Setter
//@Table(name="players")
public class Player extends User {
    @Embedded
    private BankAccount bankAccount;

    @Getter
    private static final Logger logger = LoggerFactory.getLogger(Player.class);

    @Embedded
    private ReservationCalendar reservationCalendar;

    @Builder
    public Player(Long id, String firstName, String lastName, String username, String password, Address address, String mailAddress, String phoneNumber, String phoneCode, Address billingAddress) {
        super(id, firstName, lastName, username, password, mailAddress, phoneNumber, phoneCode, address, billingAddress);
        this.bankAccount = new BankAccount(); // Předpokládáme, že BankAccount má vhodný konstruktor
        this.reservationCalendar = new ReservationCalendar();
    }

    public void addReservation(Reservation reservation) {
        reservationCalendar.addReservation(reservation);
        System.out.println(reservationCalendar.getReservations());
    }


    @Transient
    @Override
    public Collection<CustomGrantedAuthority> getAuthorities() {
        List<CustomGrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new CustomGrantedAuthority("ROLE_PLAYER"));
        return authorities;
    }

    public void updateFromDTO(UserDTO.Detailed dto) {
        this.setFirstName(dto.getFirstName());
        this.setLastName(dto.getLastName());
        this.setMailAddress(dto.getEmail());
        this.setPhoneNumber(dto.getPhoneNumber());
        this.setPhoneCode(dto.getPhoneCode());
        if (dto.getBankAccount() != null) {
            this.setBankAccount(dto.getBankAccount());
        }
        if (dto.getAddress() != null) {
            this.setAddress(dto.getAddress().toAddress()); // Přidání metody
        }
    }
}
