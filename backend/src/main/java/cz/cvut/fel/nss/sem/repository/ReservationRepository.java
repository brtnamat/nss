package cz.cvut.fel.nss.sem.repository;

import cz.cvut.fel.nss.sem.entities.Fields.SportCenter;
import cz.cvut.fel.nss.sem.entities.reservation.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface ReservationRepository extends JpaRepository<Reservation, Long> {
    @Query("SELECT r FROM Reservation r WHERE r.reservatedField.fieldId=:reservationFieldId AND r.startDateHour=:reservationStart")
    List<Reservation> findConflictingReservations(@Param("reservationFieldId") Long reservationFieldId, @Param("reservationStart") LocalDateTime reservationStart);


    @Query("SELECT r FROM Reservation r WHERE r.reservatedField.fieldId = :reservationFieldId " +
           "AND (r.startDateHour < :reservationEnd AND (r.endDateHour > :reservationStart OR r.endDateHour = :reservationEnd)) ")
    List<Reservation> findConflictingReservations(@Param("reservationFieldId") Long reservationFieldId,
                                                  @Param("reservationStart") LocalDateTime reservationStart,
                                                  @Param("reservationEnd") LocalDateTime reservationEnd);

    @Query("SELECT r FROM Reservation r WHERE r.reservationId= :reservationId")
    Reservation findByReservationId(@Param("reservationId") Long reservationId);
}

