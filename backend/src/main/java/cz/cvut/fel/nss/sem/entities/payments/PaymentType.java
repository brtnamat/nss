package cz.cvut.fel.nss.sem.entities.payments;

public enum PaymentType {
    CASH,
    BANK_TRANSFER,
    QR_CODE

}
