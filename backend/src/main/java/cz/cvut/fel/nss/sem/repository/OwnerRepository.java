package cz.cvut.fel.nss.sem.repository;

import cz.cvut.fel.nss.sem.entities.Fields.Field;
import cz.cvut.fel.nss.sem.entities.users.Owner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface OwnerRepository extends JpaRepository<Owner, Long> {
    @Query("SELECT o FROM Owner o WHERE o.userId= :userId")
    Owner findByOwnerId(@Param("userId") Long userId);

    @Query("SELECT o FROM Owner o WHERE o.username= :username")
    Owner findByOwnerUsername(@Param("username") String username);

    @Query("SELECT o FROM Owner o WHERE o.mailAddress= :email")
    Owner findByOwnerEmail(@Param("email") String email);
}
