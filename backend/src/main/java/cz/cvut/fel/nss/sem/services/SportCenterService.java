package cz.cvut.fel.nss.sem.services;

import cz.cvut.fel.nss.sem.DTO.FieldDTO;
import cz.cvut.fel.nss.sem.DTO.SportCenterDTO;
import cz.cvut.fel.nss.sem.entities.Address;
import cz.cvut.fel.nss.sem.entities.Fields.Field;
import cz.cvut.fel.nss.sem.entities.Fields.SportCenter;
import cz.cvut.fel.nss.sem.entities.users.Admin;
import cz.cvut.fel.nss.sem.entities.users.Owner;
import cz.cvut.fel.nss.sem.entities.users.User;
import cz.cvut.fel.nss.sem.repository.FieldRepository;
import cz.cvut.fel.nss.sem.repository.OwnerRepository;
import cz.cvut.fel.nss.sem.repository.SportCenterRepository;
import cz.cvut.fel.nss.sem.repository.UserRepository;
import cz.cvut.fel.nss.sem.utilities.SportCenterWrapper;
import org.apache.kafka.common.errors.LeaderNotAvailableException;
import org.apache.kafka.common.errors.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@EnableCaching
public class SportCenterService {
    private final OwnerRepository ownerRepository;
    private final SportCenterRepository sportCenterRepository;
    private final FieldRepository fieldRepository;
    private final UserRepository userRepository;

    @Autowired
    public SportCenterService(SportCenterRepository sportCenterRepository, FieldRepository fieldRepository, UserRepository userRepository,OwnerRepository ownerRepository) {
        this.sportCenterRepository = sportCenterRepository;
        this.fieldRepository = fieldRepository;
        this.userRepository = userRepository;
        this.ownerRepository=ownerRepository;
    }

    @Cacheable("sportCenters")
    public List<SportCenterDTO.Basic> getAllSportCenters() {
        List<SportCenter> sportCenters = sportCenterRepository.findAll();
        return sportCenters.stream()
                .map(SportCenterDTO.Basic::new)
                .collect(Collectors.toList());
    }

    @Cacheable(value = "sportCenter", key = "#sportCenterId")
    public SportCenterDTO.Basic getSportCenter(Long sportCenterId) {
        SportCenter sportCenter = sportCenterRepository.findById(sportCenterId)
                .orElseThrow(() -> new ResourceNotFoundException("SportCenter not found"));
        return new SportCenterDTO.Basic(sportCenter);
    }

    @CacheEvict(value = "sportCenters", allEntries = true)
    public Field addFieldToSportCenter(Long sportCenterId, Integer width, Integer length) {
        SportCenter sportCenter = sportCenterRepository.findById(sportCenterId)
                .orElseThrow(() -> new ResourceNotFoundException("SportCenter not found"));

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        User user = userRepository.findByUserUsername(username)
                .orElseThrow(() -> new ResourceNotFoundException("User not found"));

        if (!(user instanceof Owner || user instanceof Admin)) {
            throw new IllegalStateException("User not authorized");
        }

        Field newField = Field.builder()
                .width(width)
                .length(length)
                .build();
        Field savedField = fieldRepository.save(newField);

        sportCenter.addField(savedField);
        sportCenterRepository.save(sportCenter);

        return savedField;
    }

    @CacheEvict(value = "sportCenters", allEntries = true)
    public boolean addSportCenter(SportCenterWrapper requestData) {
        Map<String,String> sportCenterParams=requestData.getSportCenterParams();
        String name = sportCenterParams.get("name");
        String  city= sportCenterParams.get("city");
        String street= sportCenterParams.get("street");
        String houseNumber= sportCenterParams.get("houseNumber");
        String postalCode= sportCenterParams.get("postalCode");
        Long ownerId= Long.parseLong(sportCenterParams.get("ownerId"));
        Owner owner=ownerRepository.getById(ownerId);
        SportCenter sportCenter = SportCenter.builder()
                .name(name)
                .fields(new ArrayList<>())
                .address(Address.builder()
                        .street(street)
                        .city(city)
                        .postalCode(postalCode)
                        .houseNumber(houseNumber)
                        .build())
                .owner(owner).build();

        sportCenterRepository.save(sportCenter);
        ownerRepository.save(owner);
        ArrayList<Field> fields= requestData.getSportCenterFields();
        for (Field f: fields) {
            sportCenter.addField(f);
            fieldRepository.save(f);
        }

        return true;
    }

    @CacheEvict(value = "sportCenters", allEntries = true)
    public boolean removeSportCenter(Long sportCenterId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        User user = userRepository.findByUserUsername(username)
                .orElseThrow(() -> new ResourceNotFoundException("User not found"));

        SportCenter sportCenter = sportCenterRepository.findById(sportCenterId)
                .orElseThrow(() -> new ResourceNotFoundException("SportCenter not found"));

        if (user instanceof Owner) {
            Owner owner = (Owner) user;
            if (owner.getSportCenters().contains(sportCenter)) {
                sportCenterRepository.deleteById(sportCenterId);
                sportCenterRepository.flush();
                return true;
            }
        } else {
            sportCenterRepository.deleteById(sportCenterId);
            sportCenterRepository.flush();
            return true;
        }
        return false;
    }

    @CacheEvict(value = "sportCenters", allEntries = true)
    public void removeFieldFromSportCenter(Long sportCenterId, Long fieldId) {
        SportCenter sportCenter = sportCenterRepository.findById(sportCenterId)
                .orElseThrow(() -> new ResourceNotFoundException("SportCenter not found"));

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        User user = userRepository.findByUserUsername(username)
                .orElseThrow(() -> new ResourceNotFoundException("User not found"));

        if (user instanceof Owner) {
            Owner owner = (Owner) user;
            if (!sportCenter.getOwner().equals(owner)) {
                throw new IllegalStateException("User not authorized");
            }
        }

        sportCenter.getFields().removeIf(field -> field.getFieldId().equals(fieldId));
        sportCenterRepository.save(sportCenter);
    }

}
