package cz.cvut.fel.nss.sem.repository;

import cz.cvut.fel.nss.sem.entities.users.Player;
import cz.cvut.fel.nss.sem.entities.users.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface PlayerRepository extends JpaRepository<Player, Long> {
        @Query("SELECT p FROM Player p WHERE p.userId= :userId")
        Player findByPlayerId(@Param("userId") Long userId);

        @Query("SELECT p FROM Player p WHERE p.username= :username")
        Player findByPlayerUsername(@Param("username") String username);

        @Query("SELECT p FROM Player p WHERE p.mailAddress= :email")
        Player findByPlayerEmail(@Param("email") String email);

}
