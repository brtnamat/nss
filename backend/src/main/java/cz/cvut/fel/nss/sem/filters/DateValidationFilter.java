package cz.cvut.fel.nss.sem.filters;

import cz.cvut.fel.nss.sem.entities.reservation.Reservation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public class DateValidationFilter implements Filter<Reservation> {
    private static final Logger logger = LoggerFactory.getLogger(DateValidationFilter.class);


    @Override
    public Reservation execute(Reservation input) throws Exception {
        //todo vracet lepsi kod nez 500
        if (input.getStartDateHour() == null || input.getEndDateHour() == null) {
            throw new Exception("Datum nebo čas rezervace je neplatné!");
        }

        LocalDateTime startDate = input.getStartDateHour();
        LocalDateTime endDate = input.getEndDateHour();
        LocalDateTime now = LocalDateTime.now();



        if (startDate.isBefore(now)) {
            throw new Exception("Počáteční čas rezervace musí být v budoucnosti!");
        }

        // Zkontroluj, zda koncový čas je po počátečním čase
        if (endDate.isBefore(startDate) || endDate.equals(startDate)) {
            throw new Exception("Koncový čas rezervace musí být po počátečním čase!");
        }

        long diffInMinutes = ChronoUnit.MINUTES.between(startDate, endDate);

        // Zkontrolujeme, zda je rozdíl mezi startem a endem delitelný 60, což by znamenalo, že je rozdíl na celé hodiny
        if (diffInMinutes < 60 ) {
            throw new Exception("Zvolene datum a cas od sebe jsou mene nez hodinu!"); // Časové rozpětí je neplatné
        }
        if(diffInMinutes % 60 != 0){
            throw new Exception("Zvolene datum a cas nejsou nastavene na cele hodiny!"); // Časové rozpětí je neplatné
        }
        startDate = startDate.plusSeconds(1);
        input.setStartDateHour(startDate);
        return input;
    }
}
