package cz.cvut.fel.nss.sem.DTO;

import cz.cvut.fel.nss.sem.entities.BankAccount;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BankAccountDTO {
    private Integer number;
    private Integer codeOfBank;

    public BankAccountDTO(BankAccount bankAccount) {
        if (bankAccount != null) {
            this.number = bankAccount.getNumber();
            this.codeOfBank = bankAccount.getCodeOfBank();
        }
    }

    public BankAccount toEntity() {
        BankAccount bankAccount = new BankAccount();
        bankAccount.setNumber(this.number);
        bankAccount.setCodeOfBank(this.codeOfBank);
        return bankAccount;
    }
}
