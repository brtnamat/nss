package cz.cvut.fel.nss.sem.filters.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

// Tento RestControllerAdvice zpracovává globálně výjimky
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> handleException(Exception ex) {
        String userFriendlyMessage = ex.getMessage();

        if (userFriendlyMessage != null && userFriendlyMessage.startsWith("java.lang.Exception: ")) {
            userFriendlyMessage = userFriendlyMessage.replace("java.lang.Exception: ", "");
        }

        ErrorResponse errorResponse = new ErrorResponse(
                HttpStatus.BAD_REQUEST.value(),
                userFriendlyMessage
        );

        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }
}
