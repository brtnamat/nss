package cz.cvut.fel.nss.sem.filters;
import cz.cvut.fel.nss.sem.entities.reservation.Reservation;

import cz.cvut.fel.nss.sem.repository.FieldRepository;
import cz.cvut.fel.nss.sem.repository.PlayerRepository;
import cz.cvut.fel.nss.sem.repository.ReservationRepository;
import cz.cvut.fel.nss.sem.repository.UserRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class PaymentFilter implements Filter<Reservation>{

    @Autowired
    PlayerRepository playerRepository;

    @Autowired
    public PaymentFilter(PlayerRepository playerRepository) {
        this.playerRepository  = playerRepository;
    }

    @Override
    public Reservation execute(Reservation input) throws Exception{
        if(input.getPayment() == null){
            throw new Exception("Payment is not in request");
        }
        return input;
    }
}

