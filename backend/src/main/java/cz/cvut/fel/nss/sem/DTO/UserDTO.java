package cz.cvut.fel.nss.sem.DTO;


import cz.cvut.fel.nss.sem.entities.BankAccount;
import cz.cvut.fel.nss.sem.entities.Fields.SportCenter;
import cz.cvut.fel.nss.sem.entities.reservation.ReservationCalendar;
import cz.cvut.fel.nss.sem.entities.users.Admin;
import cz.cvut.fel.nss.sem.entities.users.Owner;
import cz.cvut.fel.nss.sem.utilities.CustomGrantedAuthority;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import cz.cvut.fel.nss.sem.entities.users.Player;
import cz.cvut.fel.nss.sem.entities.users.User;
import lombok.*;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Data
public class UserDTO {

    @Getter
    @Setter
    public static class Basic {

        private final String username;
        private final String firstName;
        private final String lastName;
        private final Long userId;


        public Basic(User user) {
            this.username = user.getUsername();
            this.firstName = user.getFirstName();
            this.lastName = user.getLastName();
            this.userId = user.getUserId();

        }

        public Basic(Owner owner) {
            this.username = owner.getUsername();
            this.firstName = owner.getFirstName();
            this.lastName = owner.getLastName();
            this.userId = owner.getUserId();
        }

        public Basic(Player player) {
            this.username = player.getUsername();
            this.firstName = player.getFirstName();
            this.lastName = player.getLastName();
            this.userId = player.getUserId();
        }
    }

    @Getter
    @Setter
    public static class Auth {
        private Long id;
        private String username;
        private String email;
        private String password;
        private Collection<CustomGrantedAuthority> authorities;

        public Auth(User user) {
            this.id = user.getUserId();
            this.username = user.getUsername();
            this.email = user.getMailAddress();
            this.password = user.getPassword();
            this.authorities = user.getAuthorities();
        }

    }

    @Getter
    @AllArgsConstructor
    public static class Updating{
        private final String username;
        private final String firstName;
        private final String lastName;
        private final String emailAddress;
        private final String password;

        public Updating(User user) {
            this.username = user.getUsername();
            this.firstName = user.getFirstName();
            this.lastName = user.getLastName();
            this.emailAddress = user.getMailAddress();
            this.password = user.getPassword();
        }
    }

    @Getter
    public static class Authorized {
        private final String username;
        private final String firstName;
        private final String lastName;
        private final Long userId;
        private final String type;
        private final String token;

        public Authorized(User user, String token) {
            this.username = user.getUsername();
            this.firstName = user.getFirstName();
            this.lastName = user.getLastName();
            this.userId = user.getUserId();
            this.type = user.getClass().getSimpleName();
            this.token = token;
        }

    }

    @Getter
    @AllArgsConstructor
    public static class Detailed {
        private final String username;
        private final String firstName;
        private final String lastName;
        private final Long userId;
        private final String email;
        private final String type;
        private final String phoneNumber;
        private final String phoneCode;
//        private ReservationCalendarDTO calendar = null;
        private ReservationCalendarDTO calendar = null;
        private List<SportCenterDTO.Basic> sportCenters = null;
        private BankAccount bankAccount = null;
        private AddressDTO address = null; // Přidání adresy, pokud je potřeba

        // Konstruktor pro generického User
        public Detailed(User user) {
            this.username = user.getUsername();
            this.firstName = user.getFirstName();
            this.lastName = user.getLastName();
            this.userId = user.getUserId();
            this.email = user.getMailAddress();
            this.type = user.getClass().getSimpleName();
            this.phoneNumber = user.getPhoneNumber();
            this.phoneCode = user.getPhoneCode();
        }

        public Detailed(Owner owner) {
            this.firstName = owner.getFirstName();
            this.username = owner.getUsername();
            this.lastName = owner.getLastName();
            this.userId = owner.getUserId();
            this.email = owner.getMailAddress();
            this.phoneNumber = owner.getPhoneNumber();
            log.info("owner");
            this.phoneCode = owner.getPhoneCode();
            this.type = owner.getClass().getSimpleName();
            this.bankAccount = owner.getBankAccount();
            if(owner.getAddress()==null) {
                this.address = null;
            }else {
                this.address = new AddressDTO(owner.getAddress());
            }
            this.sportCenters = owner.getSportCenters().stream()
                    .map(SportCenterDTO.Basic::new)
                    .collect(Collectors.toList());
        }

        public Detailed(Player player) {
            this.username = player.getUsername();
            this.firstName = player.getFirstName();
            this.lastName = player.getLastName();
            this.phoneNumber = player.getPhoneNumber();
            this.phoneCode = player.getPhoneCode();
            this.userId = player.getUserId();
            this.email = player.getMailAddress();
            this.type = player.getClass().getSimpleName();
            this.bankAccount = player.getBankAccount();
            if(player.getAddress()!=null) {
                this.address = new AddressDTO(player.getAddress());
            }
            this.calendar = new ReservationCalendarDTO(player.getReservationCalendar());
        }
    }
}
