package cz.cvut.fel.nss.sem.entities.reservation;

import jakarta.persistence.*;

import java.util.List;
import lombok.*;

@Getter
@Setter
@Embeddable
public class ReservationCalendar {
    @OneToMany
    List<Reservation> reservations;

    public void addReservation(Reservation reservation) {
        reservations.add(reservation);
    }
}
