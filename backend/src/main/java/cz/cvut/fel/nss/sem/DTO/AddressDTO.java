package cz.cvut.fel.nss.sem.DTO;
import cz.cvut.fel.nss.sem.entities.Address;
import lombok.*;


@Data
public class AddressDTO {
    private final String street;
    private final String houseNumber;
    private final String city;
    private final String postalCode;

    public AddressDTO(Address address) {
        this.street = address.getStreet();
        this.houseNumber = address.getHouseNumber();
        this.city = address.getCity();
        this.postalCode = address.getPostalCode();
    }
    public Address toAddress() {
        return new Address(this.street, this.houseNumber, this.city, this.postalCode);
    }
}
