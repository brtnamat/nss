package cz.cvut.fel.nss.sem.filters;

import cz.cvut.fel.nss.sem.entities.reservation.Reservation;
import cz.cvut.fel.nss.sem.repository.ReservationRepository;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AvailabilityFilter implements Filter<Reservation> {
    private static final Logger logger = LoggerFactory.getLogger(AvailabilityFilter.class);

    private final ReservationRepository reservationRepository;

    @Autowired
    public AvailabilityFilter(ReservationRepository reservationRepository) {
        this.reservationRepository = reservationRepository;
    }

    @Override
    public Reservation execute(Reservation input) throws Exception{
        logger.info("Entering execute method availability");

        boolean isAvailable = checkAvailability(input);
        if (!isAvailable) {
            logger.info("Reservation is not available");
            throw new Exception("Reservation is not available");
        }

        logger.info("Reservation is available and is being processed");
        return input;
    }



    private boolean checkAvailability(Reservation reservation) {
        logger.info("je to ve funkci check availibility");
        return reservationRepository.findConflictingReservations(reservation.getReservatedField().getFieldId(),reservation.getStartDateHour(),reservation.getEndDateHour()).isEmpty();
    }
}
