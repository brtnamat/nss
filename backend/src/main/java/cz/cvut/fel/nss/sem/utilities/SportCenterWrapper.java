package cz.cvut.fel.nss.sem.utilities;

import cz.cvut.fel.nss.sem.DTO.FieldDTO;
import cz.cvut.fel.nss.sem.entities.Fields.Field;
import cz.cvut.fel.nss.sem.entities.Fields.SportCenter;
import lombok.Data;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Map;

@Getter
@Data
public class SportCenterWrapper {
    private Map<String,String> sportCenterParams;
    private ArrayList<Field> sportCenterFields;
}
