package cz.cvut.fel.nss.sem.filters;

import cz.cvut.fel.nss.sem.entities.Fields.SportCenter;
import cz.cvut.fel.nss.sem.entities.reservation.Reservation;
import cz.cvut.fel.nss.sem.entities.users.Player;
import cz.cvut.fel.nss.sem.entities.reservation.*;
import cz.cvut.fel.nss.sem.entities.users.User;
import cz.cvut.fel.nss.sem.repository.PlayerRepository;
import org.apache.kafka.common.errors.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class UserValidationFilter implements Filter<Reservation> {
        private final PlayerRepository playerRepository;
    private static final Logger logger = LoggerFactory.getLogger(UserValidationFilter.class);

    public UserValidationFilter(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }


    @Override
    public Reservation execute(Reservation input) throws Exception {
        try{
            input.getReservatingPlayer().getUserId();
        }
        catch (Exception e){
            throw new Exception("Uzivatel musi byt hrac, aby mohl vytvorit rezervaci");
        }

        playerRepository.findById(input.getReservatingPlayer().getUserId())
                .orElseThrow(() -> new ResourceNotFoundException("Player with id " + input.getReservatingPlayer().getUserId() + " not found"));
        return input;
    }
}
