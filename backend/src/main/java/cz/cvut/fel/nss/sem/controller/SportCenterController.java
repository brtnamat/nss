package cz.cvut.fel.nss.sem.controller;

import cz.cvut.fel.nss.sem.DTO.SportCenterDTO;
import cz.cvut.fel.nss.sem.entities.Fields.Field;
import cz.cvut.fel.nss.sem.entities.Fields.SportCenter;
import cz.cvut.fel.nss.sem.services.SportCenterService;
import cz.cvut.fel.nss.sem.utilities.SportCenterWrapper;
import org.apache.kafka.common.errors.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Objects;

@RestController
@RequestMapping("/api/sport-centers")
public class SportCenterController {

    private final SportCenterService sportCenterService;

    @Autowired
    public SportCenterController(SportCenterService sportCenterService) {
        this.sportCenterService = sportCenterService;
    }

    @CrossOrigin
    @GetMapping
    public ResponseEntity<List<SportCenterDTO.Basic>> fetchAllSportCenters() {
        List<SportCenterDTO.Basic> sportCenters = sportCenterService.getAllSportCenters();
        return ResponseEntity.ok(sportCenters);
    }

    @PreAuthorize("hasAuthority('ROLE_OWNER') || hasAuthority('ROLE_ADMIN')")
    @CrossOrigin
    @PostMapping("/{sportCenterId}/addField")
    public ResponseEntity<Field> addFieldToSportCenter(
            @PathVariable Long sportCenterId,
            @RequestParam Integer width,
            @RequestParam Integer length) {
        Field field = sportCenterService.addFieldToSportCenter(sportCenterId, width, length);
        return ResponseEntity.ok(field);
    }

    @PreAuthorize("hasAuthority('ROLE_OWNER') || hasAuthority('ROLE_ADMIN')")
    @CrossOrigin
    @PostMapping("/addSportCenter")
    public ResponseEntity<Boolean> addSportCenter(@RequestBody SportCenterWrapper requestData) {
        boolean result = sportCenterService.addSportCenter(requestData);
        return ResponseEntity.ok(result);
    }

    @PreAuthorize("hasAuthority('ROLE_OWNER') || hasAuthority('ROLE_ADMIN')")
    @CrossOrigin
    @DeleteMapping("/removeSportCenter")
    public ResponseEntity<Boolean> removeSportCenter(@RequestParam Long sportCenterId) throws ResourceNotFoundException {
        boolean result = sportCenterService.removeSportCenter(sportCenterId);
        return ResponseEntity.ok(result);
    }

    @PreAuthorize("hasAuthority('ROLE_OWNER') || hasAuthority('ROLE_ADMIN')")
    @CrossOrigin
    @DeleteMapping("/{sportCenterId}/removeField/{fieldId}")
    public ResponseEntity<Void> removeFieldFromSportCenter(
            @PathVariable Long sportCenterId,
            @PathVariable Long fieldId) {
        sportCenterService.removeFieldFromSportCenter(sportCenterId, fieldId);
        return ResponseEntity.ok().build();
    }

    @CrossOrigin
    @GetMapping("/{sportCenterId}")
    public ResponseEntity<SportCenterDTO.Basic> getSportCenter(@PathVariable Long sportCenterId) {
        SportCenterDTO.Basic sportCenter = sportCenterService.getSportCenter(sportCenterId);
        return ResponseEntity.ok(sportCenter);
    }
}
