package cz.cvut.fel.nss.sem.DTO;

import cz.cvut.fel.nss.sem.entities.users.Player;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PlayerDTO {
    private Long playerId;
    private String firstName;
    private String lastName;
    private String username;
    private String mailAddress;
    private String phoneNumber;
    private BankAccountDTO bankAccount;
    private ReservationCalendarDTO reservationCalendar;


    @Data
    public static final class Basic {
        private Long playerId;
        private String firstName;
        private String lastName;
        private String username;

        public Basic(Player player) {
            this.playerId = player.getUserId();
            this.firstName = player.getFirstName();
            this.lastName = player.getLastName();
            this.username = player.getUsername();
        }
    }



    @Data
    public static final class Detailed {
        private Long playerId;
        private String firstName;
        private String lastName;
        private String username;
        private String mailAddress;
        private String phoneNumber;
        private BankAccountDTO bankAccount;
        private ReservationCalendarDTO reservationCalendar;

        public Detailed(Player player) {
            this.playerId = player.getUserId();
            this.firstName = player.getFirstName();
            this.lastName = player.getLastName();
            this.username = player.getUsername();
            this.mailAddress = player.getMailAddress();
            this.phoneNumber = player.getPhoneNumber();
            this.bankAccount = new BankAccountDTO(player.getBankAccount());
            this.reservationCalendar = new ReservationCalendarDTO(player.getReservationCalendar());
        }
    }
}
