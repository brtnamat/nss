package cz.cvut.fel.nss.sem.entities.Fields;

import cz.cvut.fel.nss.sem.DTO.SportCenterDTO;
import cz.cvut.fel.nss.sem.entities.reservation.Reservation;
import cz.cvut.fel.nss.sem.entities.reservation.ReservationCalendar;
import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@AllArgsConstructor
@Builder
@Getter
@Setter
@Table(name = "fields")
public class Field {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long fieldId;

    @Column(name = "width")
    private Integer width;

    @Column(name="hourRate")
    private Integer hourRate;

    @ManyToOne
    @JoinColumn(name = "sportCenterId")
    private SportCenter sportCenter;

    @Column(name = "length")
    private Integer length;

    @Embedded
    private ReservationCalendar reservationCalendar;

    public Field() {
        this.reservationCalendar = new ReservationCalendar();
    }

    public Field(Integer width, Integer length, Integer hourRate) {
        this.width = width;
        this.length = length;
        this.hourRate=hourRate;
        this.reservationCalendar = new ReservationCalendar();
    }

    public void addReservation(Reservation reservation){
        reservationCalendar.addReservation(reservation);
    }
}
