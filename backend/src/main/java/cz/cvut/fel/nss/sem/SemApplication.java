package cz.cvut.fel.nss.sem;

import cz.cvut.fel.nss.sem.entities.Address;
import cz.cvut.fel.nss.sem.entities.Fields.Field;
import cz.cvut.fel.nss.sem.entities.Fields.SportCenter;
import cz.cvut.fel.nss.sem.entities.users.Admin;
import cz.cvut.fel.nss.sem.entities.users.Owner;
import cz.cvut.fel.nss.sem.entities.users.Player;
import cz.cvut.fel.nss.sem.repository.FieldRepository;
import cz.cvut.fel.nss.sem.repository.SportCenterRepository;
import cz.cvut.fel.nss.sem.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import static java.lang.System.Logger.Level.DEBUG;

@SpringBootApplication
@EnableCaching
public class SemApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(SemApplication.class, args);
    }

    @Autowired
    private FieldRepository fieldRepository;

    @Autowired
    private SportCenterRepository sportCenterRepository;

    @Autowired
    private UserRepository userRepository;


    @Override
    public void run (String[] args) {
//         Vytvoření a uložení Ownera
//        Owner owner1 = Owner.builder()
//                .firstName("Jane")
//                .lastName("Smith")
//                .username("janesmith")
//                .password("password456")
//                .mailAddress("jane.smith@example.com")
//                .phoneNumber("987654321")
//                .phoneCode("5678")
//                .address(Address.builder()
//                        .street("Broadway")
//                        .houseNumber("456")
//                        .city("Los Angeles")
//                        .postalCode("90001")
//                        .build())
//                .build();
//        userRepository.save(owner1);
//
//        SportCenter sportCenter = SportCenter.builder()
//                .fields(new ArrayList<>())
//                .address(Address.builder()
//                        .street("Main Street")
//                        .houseNumber("123")
//                        .city("New York")
//                        .postalCode("10001")
//                        .build())
//                .owner(owner1).build();
//        sportCenterRepository.save(sportCenter);
//        owner1.addSportCenter(sportCenter);
//        userRepository.save(owner1);
//
//        Field field1 = Field.builder().width(250).length(250).sportCenter(sportCenter).build();
//        Field field2 = Field.builder().width(200).length(220).sportCenter(sportCenter).build();
//        Field field3 = Field.builder().width(150).length(200).sportCenter(sportCenter).build();
//
//        fieldRepository.save(field1);
//        fieldRepository.save(field2);
//        fieldRepository.save(field3);
//
//
//
////
//        Admin admin1 = Admin.builder()
//                .firstName("John")
//                .lastName("Doe")
//                .username("johndoe")
//                .password("password123")
//                .mailAddress("john.doe@example.com")
//                .phoneNumber("123456789")
//                .phoneCode("1234")
//                .address(Address.builder()
//                        .street("Main Street")
//                        .houseNumber("123")
//                        .city("New York")
//                        .postalCode("10001")
//                        .build())
//                .build();
//
//        userRepository.save(admin1);
//
//
//
//        // Vytvoření a uložení Playera
//        Player player1 = Player.builder()
//                .firstName("John")
//                .lastName("Doe")
//                .username("johndoeplayer")
//                .password("password123")
//                .mailAddress("john.doe@example.com")
//                .phoneNumber("123456789")
//                .phoneCode("1234")
//                .address(Address.builder()
//                        .street("Main Street")
//                        .houseNumber("123")
//                        .city("New York")
//                        .postalCode("10001")
//                        .build())
//                .build();
//
//
//        userRepository.save(player1);
    }
}
