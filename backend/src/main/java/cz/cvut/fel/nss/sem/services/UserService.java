package cz.cvut.fel.nss.sem.services;

import cz.cvut.fel.nss.sem.DTO.UserDTO;
import cz.cvut.fel.nss.sem.entities.Address;
import cz.cvut.fel.nss.sem.entities.reservation.ReservationCalendar;
import cz.cvut.fel.nss.sem.entities.users.Owner;
import cz.cvut.fel.nss.sem.entities.users.Player;
import cz.cvut.fel.nss.sem.entities.users.User;
import cz.cvut.fel.nss.sem.repository.OwnerRepository;
import cz.cvut.fel.nss.sem.repository.PlayerRepository;
import cz.cvut.fel.nss.sem.repository.UserRepository;
import cz.cvut.fel.nss.sem.utilities.AuthRequest;
import cz.cvut.fel.nss.sem.utilities.JwtService;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.errors.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Slf4j
@Service
public class UserService {

    @Autowired
    private JwtService jwtService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PlayerRepository playerRepository;

    @Autowired
    private OwnerRepository ownerRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Cacheable(value = "users", key = "#username")
    public UserDTO.Detailed getUserProfile(String username) {
        User user = userRepository.findByUserUsername(username).orElse(null);
        if (user == null) {
            return null;
        }
        return new UserDTO.Detailed(user);
    }



    @CacheEvict(value = "users", key = "#userDetails.getUsername()")
    public UserDTO.Detailed updateUserProfile(UserDTO.Detailed userDetails) {
        String username = userDetails.getUsername();
        log.info("{}", userDetails);

        User user = userRepository.findByUserUsername(username).orElse(null);
        if (user == null) {
            return null;
        }
        log.info("here i am");
        if (user instanceof Player) {
            Player player = (Player) user;
            log.info("player");

            if (userDetails.getFirstName() != null && !userDetails.getFirstName().isEmpty()) {
                player.setFirstName(userDetails.getFirstName());
            }
            if (userDetails.getLastName() != null && !userDetails.getLastName().isEmpty()) {
                player.setLastName(userDetails.getLastName());
            }
            if (userDetails.getEmail() != null && !userDetails.getEmail().isEmpty()) {
                player.setMailAddress(userDetails.getEmail());
            }
            if (userDetails.getPhoneNumber() != null && !userDetails.getPhoneNumber().isEmpty()) {
                player.setPhoneNumber(userDetails.getPhoneNumber());
            }
            if (userDetails.getPhoneCode() != null && !userDetails.getPhoneCode().isEmpty()) {
                player.setPhoneCode(userDetails.getPhoneCode());
            }
            if (userDetails.getBankAccount() != null) {
                player.setBankAccount(userDetails.getBankAccount());
            }
            if (userDetails.getAddress() != null) {
                player.setAddress(userDetails.getAddress().toAddress());
            }
            userRepository.save(player);
            log.info("{}", player);
            return new UserDTO.Detailed(player);

        } else if (user instanceof Owner) {
            Owner owner = (Owner) user;
            log.info("owner");

            if (userDetails.getFirstName() != null && !userDetails.getFirstName().isEmpty()) {
                owner.setFirstName(userDetails.getFirstName());
            }
            if (userDetails.getLastName() != null && !userDetails.getLastName().isEmpty()) {
                owner.setLastName(userDetails.getLastName());
            }
            if (userDetails.getEmail() != null && !userDetails.getEmail().isEmpty()) {
                owner.setMailAddress(userDetails.getEmail());
            }
            if (userDetails.getPhoneNumber() != null && !userDetails.getPhoneNumber().isEmpty()) {
                owner.setPhoneNumber(userDetails.getPhoneNumber());
            }
            if (userDetails.getPhoneCode() != null && !userDetails.getPhoneCode().isEmpty()) {
                owner.setPhoneCode(userDetails.getPhoneCode());
            }
            if (userDetails.getBankAccount() != null) {
                owner.setBankAccount(userDetails.getBankAccount());
            }
            if (userDetails.getAddress() != null) {
                owner.setAddress(userDetails.getAddress().toAddress());
            }

            userRepository.save(owner);
            log.info("{}", owner);
            return new UserDTO.Detailed(owner);


        }
         else {
            return null;
        }
    }

//    @Cacheable(value = "reservationCalendars", key = "#username")
    public ReservationCalendar getPlayerReservationCalendar(String username) {
        Player player = playerRepository.findByPlayerUsername(username);
        return player.getReservationCalendar();
    }

    @Cacheable(value = "users", key = "#id")
    public UserDTO.Detailed getUserById(Long id) {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User not found"));

        if (user instanceof Player) {
            return new UserDTO.Detailed(playerRepository.findByPlayerId(id));
        } else if (user instanceof Owner) {
            return new UserDTO.Detailed(ownerRepository.findByOwnerId(id));
        } else {
            return new UserDTO.Detailed(user);
        }
    }

    @Cacheable(value = "users")
    public List<UserDTO.Basic> fetchAllUsers() {
        List<User> users = userRepository.findAll();
        return users.stream()
                .map(UserDTO.Basic::new)
                .collect(Collectors.toList());
    }

    @CacheEvict(value = "users", allEntries = true)
    public boolean registerPlayer(Map<String, String> requestData) {
        String username = requestData.get("username");
        String password = requestData.get("password");
        String confirmPassword = requestData.get("confirmPassword");
        String email = requestData.get("email");

        checkCollidingUser(username);
        checkCollidingPlayerEmail(email);
        checkUserDetailsRegex(username, email);
        checkPasswords(password, confirmPassword);

        Player player = new Player(1L, null, null, username, passwordEncoder.encode(password), null, email, null, null, null);
        playerRepository.save(player);
        return true;
    }

    @CacheEvict(value = "users", allEntries = true)
    public boolean registerOwner(Map<String, String> requestData) {
        String username = requestData.get("username");
        String password = requestData.get("password");
        String confirmPassword = requestData.get("confirmPassword");
        String email = requestData.get("email");

        checkCollidingUser(username);
        checkCollidingOwnerEmail(email);
        checkUserDetailsRegex(username, email);
        checkPasswords(password, confirmPassword);

        Owner owner = new Owner(1L, null, null, username, passwordEncoder.encode(password), null, email, null, null, null);
        ownerRepository.save(owner);
        return true;
    }

    @CacheEvict(value = "users", key = "#authRequest.getUsername()")
    public String authenticateAndGetToken(AuthRequest authRequest) {
        User userPre = userRepository.findByUserUsername(authRequest.getUsername()).orElseThrow(() -> new UsernameNotFoundException("Invalid user request!"));
        if (passwordEncoder.matches(authRequest.getPassword(), userPre.getPassword())) {
            return jwtService.generateToken(userPre.getUsername(), userPre.getUserId(), userPre.getAuthorities());
        } else {
            throw new IllegalArgumentException("Wrong password");
        }
    }

    private void checkCollidingUser(String username) {
        Optional<User> collidingUser = userRepository.findByUserUsername(username);
        if (collidingUser.isPresent()) {
            throw new IllegalArgumentException("User with this username already exists");
        }
    }

    private void checkCollidingOwnerEmail(String email) {
        Owner collidingOwner = ownerRepository.findByOwnerEmail(email);
        if (collidingOwner != null) {
            throw new IllegalArgumentException("Owner with this email already exists");
        }
    }

    private void checkCollidingPlayerEmail(String email) {
        Player collidingPlayer = playerRepository.findByPlayerEmail(email);
        if (collidingPlayer != null) {
            throw new IllegalArgumentException("Player with this email already exists");
        }
    }

    private void checkUserDetailsRegex(String username, String email) {
        if (username.length() < 5 || username.length() > 32) {
            throw new IllegalArgumentException("Username is shorter than 5 or longer than 32 characters");
        }

        final String EMAIL_REGEX = "^[\\w._%+-]+@[\\w.-]+\\.[a-zA-Z]{2,}$";
        Pattern pattern = Pattern.compile(EMAIL_REGEX);
        Matcher matcher = pattern.matcher(email);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("Email is in the wrong format");
        }
    }

    private void checkPasswords(String password, String confirmPassword) {
        if (!password.equals(confirmPassword)) {
            throw new IllegalArgumentException("Passwords do not match");
        }
        int length = password.length();
        if (32 < length || length < 8) {
            throw new IllegalArgumentException("Invalid length of password");
        }

        if (!(password.matches(".*\\d.*") && password.matches(".*[A-Z].*") && password.matches(".*[a-z].*"))) {
            throw new IllegalArgumentException("Password has to contain number, lower case and upper case");
        }
    }
}
