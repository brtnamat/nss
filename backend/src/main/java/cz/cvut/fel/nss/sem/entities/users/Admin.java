package cz.cvut.fel.nss.sem.entities.users;

import cz.cvut.fel.nss.sem.entities.*;
import cz.cvut.fel.nss.sem.utilities.CustomGrantedAuthority;
import jakarta.persistence.*;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

//@AllArgsConstructor
@NoArgsConstructor
@DiscriminatorValue("ADMIN")
@Entity
@Getter
@Setter
//@Table(name="admins")
public class Admin extends User{
    @Builder
    public Admin(Long id, String firstName, String lastName, String username, String password, String mailAddress, String phoneNumber, String phoneCode, Address address, Address billingAddress) {
        super(id, firstName, lastName, username, password, mailAddress, phoneNumber, phoneCode, address, billingAddress);
    }

    @Transient
    @Override
    public Collection<CustomGrantedAuthority> getAuthorities() {
        List<CustomGrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new CustomGrantedAuthority("ROLE_ADMIN"));
        return authorities;
    }


}
