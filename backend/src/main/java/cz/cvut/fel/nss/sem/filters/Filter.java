package cz.cvut.fel.nss.sem.filters;

import cz.cvut.fel.nss.sem.entities.reservation.*;
import org.springframework.http.ResponseEntity;

public interface Filter<T>{

    /**
     * Method to apply a filter on the given entity.
     *
//     * @param entity The entity (e.g., Reservation, Payment, User) to apply the filter to.
     * @return Modified object after filter has been applied.
     */
    T execute(T input) throws Exception;

}