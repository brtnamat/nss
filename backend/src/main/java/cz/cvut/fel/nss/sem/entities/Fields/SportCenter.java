package cz.cvut.fel.nss.sem.entities.Fields;

import cz.cvut.fel.nss.sem.entities.Address;
import cz.cvut.fel.nss.sem.entities.reservation.Reservation;
import cz.cvut.fel.nss.sem.entities.users.Owner;
import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Getter
@Setter
@Builder
public class SportCenter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long sportCenterId;

    @Column(name = "name")
    private String name;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "sportCenterId")
    @Builder.Default
    private List<Field> fields = new ArrayList<>();

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "street", column = @Column(name = "sportsCenterStreet")),
            @AttributeOverride(name = "houseNumber", column = @Column(name = "sportsCenterHouseNumber")),
            @AttributeOverride(name = "city", column = @Column(name = "sportsCenterCity")),
            @AttributeOverride(name = "postalCode", column = @Column(name = "sportsCenterPostalCode"))
    })
    Address address;

    @ManyToOne
    @JoinColumn(name = "userId", nullable = false)
    Owner owner;


    public Field findField(Long id) {
        for(Field field : fields) {
            if (field.getFieldId().equals(id)) {
                return field;
            }
        }
        return null;
    }

    public void addField(Field field) {
        this.fields.add(field);
    }

    public void addFields(List<Field> fields) {
        this.fields.addAll(fields);
    }

    public void removeField(Field field) {
        this.fields.remove(field);
    }

}
