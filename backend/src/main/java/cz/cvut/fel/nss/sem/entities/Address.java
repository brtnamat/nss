package cz.cvut.fel.nss.sem.entities;

import cz.cvut.fel.nss.sem.entities.users.User;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.*;
import lombok.NoArgsConstructor;

@Getter
@Setter
@Embeddable
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Address {
    String  city;
    String street;
    String houseNumber;
    String postalCode;
}
