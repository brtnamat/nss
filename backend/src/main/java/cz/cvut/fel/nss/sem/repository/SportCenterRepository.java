package cz.cvut.fel.nss.sem.repository;

import cz.cvut.fel.nss.sem.entities.Fields.SportCenter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;


public interface SportCenterRepository extends JpaRepository<SportCenter, Long> {
    @Query("SELECT sc FROM SportCenter sc WHERE sc.sportCenterId= :id")
    Optional<SportCenter> findBySportCenterId(@Param("id") Long id);
}
