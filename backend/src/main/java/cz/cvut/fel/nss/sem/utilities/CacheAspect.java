package cz.cvut.fel.nss.sem.utilities;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

@Slf4j
@Aspect
@Component
public class CacheAspect {

    @Pointcut("@annotation(cacheable)")
    public void cacheableMethods(Cacheable cacheable) {}

    @Before("cacheableMethods(cacheable)")
    public void beforeCacheableMethod(JoinPoint joinPoint, Cacheable cacheable) {
        log.info("Cacheable method called: {}", joinPoint.getSignature().toShortString());
    }
}
