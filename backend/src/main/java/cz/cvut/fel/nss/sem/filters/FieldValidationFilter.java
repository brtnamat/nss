package cz.cvut.fel.nss.sem.filters;

import cz.cvut.fel.nss.sem.entities.Fields.Field;
import cz.cvut.fel.nss.sem.entities.reservation.Reservation;
import cz.cvut.fel.nss.sem.entities.users.Player;
import cz.cvut.fel.nss.sem.repository.FieldRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class FieldValidationFilter implements Filter<Reservation>{
    private final FieldRepository fieldRepository;
    private static final Logger logger = LoggerFactory.getLogger(FieldValidationFilter.class);

    public FieldValidationFilter(FieldRepository fieldRepository) {
        this.fieldRepository = fieldRepository;
    }


    @Override
    public Reservation execute(Reservation input) throws Exception {
        logger.info("Entering execute method field");

        // Find the field by ID
        Field field = fieldRepository.findByFieldId(input.getReservatedField().getFieldId());
        if (field == null) {
            logger.warn("Field with ID {} not found", input.getReservatedField().getFieldId());
            throw new Exception("Field with ID " + input.getReservatedField().getFieldId() + " not found");

        }
        return input; // 200 OK
    }

}
