package cz.cvut.fel.nss.sem.repository;

import cz.cvut.fel.nss.sem.entities.Fields.Field;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface FieldRepository extends JpaRepository<Field, Long> {
        @Query("SELECT f FROM Field f WHERE f.length = :length AND f.width = :width")
        List<Field> findByWidthAndLength(@Param("length") Integer length, @Param("width") Integer width);

        @Query("SELECT f FROM Field f WHERE f.fieldId= :fieldId")
        Field findByFieldId(@Param("fieldId") Long fieldId);
}
