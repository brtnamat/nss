package cz.cvut.fel.nss.sem.controller;

import cz.cvut.fel.nss.sem.DTO.ReservationCalendarDTO;
import cz.cvut.fel.nss.sem.DTO.UserDTO;
import cz.cvut.fel.nss.sem.entities.reservation.ReservationCalendar;
import cz.cvut.fel.nss.sem.services.UserService;
import cz.cvut.fel.nss.sem.utilities.AuthRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/users")
@Slf4j
public class UserController {

    @Autowired
    private UserService userService;

    @PreAuthorize("hasAuthority('ROLE_PLAYER') || hasAuthority('ROLE_OWNER')")
    @CrossOrigin
    @GetMapping("/auth/profilePage")
    public ResponseEntity<UserDTO.Detailed> getUserProfile(@AuthenticationPrincipal UserDTO.Auth userDetails) {
        UserDTO.Detailed userDTO = userService.getUserProfile(userDetails.getUsername());
        if (userDTO == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(userDTO);
    }

    @PreAuthorize("hasAuthority('ROLE_PLAYER') || hasAuthority('ROLE_OWNER') || hasAuthority('ROLE_ADMIN')")
    @CrossOrigin
    @PostMapping("/auth/updateProfile")
    public ResponseEntity<UserDTO.Detailed> updateProfile(@RequestBody UserDTO.Detailed userDetails) {
        log.info("{}", userDetails);
//        return ResponseEntity.ok(true);
        UserDTO.Detailed updatedUser = userService.updateUserProfile(userDetails);
        if (updatedUser == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(updatedUser);
    }

    @PreAuthorize("hasAuthority('ROLE_PLAYER') || hasAuthority('ROLE_OWNER')")
    @CrossOrigin
    @GetMapping("/auth/reservationCalendar")
    public ResponseEntity<ReservationCalendarDTO> getReservationCalendar(@AuthenticationPrincipal UserDTO.Auth userDetails) {
        ReservationCalendar calendar = userService.getPlayerReservationCalendar(userDetails.getUsername());
        ReservationCalendarDTO reservationCalendarDTO = new ReservationCalendarDTO(calendar);
        return ResponseEntity.ok(reservationCalendarDTO);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN') || hasAuthority('ROLE_OWNER') || hasAuthority('ROLE_PLAYER')")
    @CrossOrigin
    @GetMapping("/auth/{id}")
    public ResponseEntity<UserDTO.Detailed> getUserById(@PathVariable Long id) {
        UserDTO.Detailed userDTO = userService.getUserById(id);
        if (userDTO == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(userDTO);
    }

    @CrossOrigin
    @GetMapping
    public ResponseEntity<List<UserDTO.Basic>> fetchAllUsers() {
        List<UserDTO.Basic> users = userService.fetchAllUsers();
        return ResponseEntity.ok(users);
    }

    @CrossOrigin
    @PostMapping("/player-registration")
    public ResponseEntity<Boolean> registerPlayer(@RequestBody Map<String, String> requestData) {
        return ResponseEntity.ok(userService.registerPlayer(requestData));
    }

    @CrossOrigin
    @PostMapping("/owner-registration")
    public ResponseEntity<Boolean> registerOwner(@RequestBody Map<String, String> requestData) {
        return ResponseEntity.ok(userService.registerOwner(requestData));
    }

    @CrossOrigin
    @PostMapping("/generate-token")
    public ResponseEntity<String> authenticateAndGetToken(@RequestBody AuthRequest authRequest) {
        String token = userService.authenticateAndGetToken(authRequest);
        return ResponseEntity.ok(token);
    }

}
