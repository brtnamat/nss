package cz.cvut.fel.nss.sem.configs;

import com.hazelcast.config.Config;
import com.hazelcast.config.MapConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HazelcastConfig {

    @Bean
    public HazelcastInstance hazelcastInstance() {
        Config config = new Config();
        config.setInstanceName("hazelcast-instance");

        // Konfigurace mapy pro session management
        MapConfig sessionMapConfig = new MapConfig();
        sessionMapConfig.setName("spring-session");
        sessionMapConfig.setTimeToLiveSeconds(3600); // TTL pro session mapu

        // Konfigurace mapy pro caching
        MapConfig cacheMapConfig = new MapConfig();
        cacheMapConfig.setName("cache");
        cacheMapConfig.setTimeToLiveSeconds(600); // TTL pro cache mapu

        config.addMapConfig(sessionMapConfig);
        config.addMapConfig(cacheMapConfig);

        return Hazelcast.newHazelcastInstance(config);
    }

}
