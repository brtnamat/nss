package cz.cvut.fel.nss.sem.entities.users;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.cvut.fel.nss.sem.DTO.UserDTO;
import cz.cvut.fel.nss.sem.entities.*;
import cz.cvut.fel.nss.sem.entities.Fields.*;
import cz.cvut.fel.nss.sem.utilities.CustomGrantedAuthority;
import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@DiscriminatorValue("OWNER")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
//@Table(name = "owners")
public class Owner extends User{
    @Embedded
    BankAccount bankAccount;

    @OneToMany(mappedBy = "owner", cascade = CascadeType.ALL)
    List<SportCenter> sportCenters = new ArrayList<>();;

    @Builder
    public Owner(Long id, String firstName, String lastName, String username, String password, Address address, String mailAddress, String phoneNumber, String phoneCode, Address billingAddress) {
        super(id, firstName, lastName, username, password, mailAddress, phoneNumber, phoneCode, address, billingAddress);
        bankAccount = new BankAccount();
    }

    public void addSportCenter(SportCenter center) {
        sportCenters.add(center);
        center.setOwner(this);
    }

    @Transient
    @Override
    public Collection<CustomGrantedAuthority> getAuthorities() {
        List<CustomGrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new CustomGrantedAuthority("ROLE_OWNER"));
        return authorities;
    }

    public void updateFromDTO(UserDTO.Detailed dto) {
        this.setFirstName(dto.getFirstName());
        this.setLastName(dto.getLastName());
        this.setMailAddress(dto.getEmail());
        this.setPhoneNumber(dto.getPhoneNumber());
        this.setPhoneCode(dto.getPhoneCode());
        if (dto.getBankAccount() != null) {
            this.setBankAccount(dto.getBankAccount());
        }
        if (dto.getAddress() != null) {
            this.setAddress(dto.getAddress().toAddress()); // Přidání metody
        }
    }
}
