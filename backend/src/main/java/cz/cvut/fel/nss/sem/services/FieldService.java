package cz.cvut.fel.nss.sem.services;

import cz.cvut.fel.nss.sem.DTO.ReservationCalendarDTO;
import cz.cvut.fel.nss.sem.entities.reservation.ReservationCalendar;
import cz.cvut.fel.nss.sem.repository.FieldRepository;
import cz.cvut.fel.nss.sem.repository.SportCenterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FieldService {

    private final FieldRepository fieldRepository;

    @Autowired
    public FieldService(FieldRepository fieldRepository) {
        this.fieldRepository = fieldRepository;
    }
    public ReservationCalendarDTO getFieldReservations(Long field_id) {
        ReservationCalendar resCal = fieldRepository.findByFieldId(field_id).getReservationCalendar();
        return new ReservationCalendarDTO(resCal);
    }
}