package cz.cvut.fel.nss.sem.controller;

import cz.cvut.fel.nss.sem.DTO.ReservationCalendarDTO;
import cz.cvut.fel.nss.sem.services.FieldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/fields")
public class FieldController {

    private final FieldService fieldService;

    @Autowired
    public FieldController(FieldService fieldService) {
        this.fieldService = fieldService;
    }

    @GetMapping(path = "getReservations/{fieldId}")
    public ResponseEntity<ReservationCalendarDTO> getFieldReservations(@PathVariable Long fieldId) {
        ReservationCalendarDTO reservationCalendarDTO = fieldService.getFieldReservations(fieldId);
        return ResponseEntity.ok(reservationCalendarDTO);
    }
}
