package cz.cvut.fel.nss.sem.pipes;

import cz.cvut.fel.nss.sem.entities.reservation.Reservation;
import cz.cvut.fel.nss.sem.filters.Filter;

import java.util.ArrayList;
import java.util.List;

public class Pipe<T> {
    private final List<Filter<T>> filters;
    private final T object;

    public Pipe(List<Filter<T>> filters, T object) {
        this.filters = filters;
        this.object = object;
    }

    public boolean fire() throws Exception {
        for (Filter<T> filter : filters) {
            try {
                if(filter.execute(object) == null){
                    return false;
                }
            } catch (Exception e) {
                throw new Exception(e);
            }
        }
        return true;
    }
}
