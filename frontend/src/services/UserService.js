import apiClient from "@/services/apiClient";

const USERS_API_BASE_URL = 'http://localhost:8081/api/users'

class UserService{

    registerOwner(owner){
        const url = `${USERS_API_BASE_URL}/owner-registration`;
        return apiClient.post(url, owner)
    }

    registerPlayer(player){
        const url = `${USERS_API_BASE_URL}/player-registration`;
        return apiClient.post(url, player)
    }

    async generateJWTToken(user) {
        try {
            const response = await apiClient.post(`${USERS_API_BASE_URL}/generate-token`, user);
            return response;
        } catch (error) {
            console.error("Error in generateJWTToken:", error);
            throw error;
        }
    }

    getUserById(userId){
        return apiClient.get(`${USERS_API_BASE_URL}/auth/${userId}`);
    }

    updatePlayerProfile(userId, updatedDetails) {
        const url = `${USERS_API_BASE_URL}/auth/updateProfile`;
        return apiClient.post(url, { user_id: userId, ...updatedDetails });
    }
}

export default new UserService()