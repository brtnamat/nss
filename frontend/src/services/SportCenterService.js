import apiClient from "@/services/apiClient";

const SPORT_CENTER_API_BASE_URL = 'http://localhost:8081/api/sport-centers'

class SportCenterService{
    getAllSportCenters(){
        return apiClient.get(SPORT_CENTER_API_BASE_URL)
    }

    getSportCenterById(sportCenterId){
        return apiClient.get(`${SPORT_CENTER_API_BASE_URL}/${sportCenterId}`);
    }

    addSportCenter(newSportCenter){
        return apiClient.post(SPORT_CENTER_API_BASE_URL + "/addSportCenter", newSportCenter)
    }

    addFieldToSportCenter(sportCenterId, width, length){
        const url = `${SPORT_CENTER_API_BASE_URL}/${sportCenterId}/addField`;
        const fieldData = {
            width: width,
            length: length
        };
        return apiClient.post(url, fieldData)
    }

    removeFieldFromSportCenter(sportCenterId, fieldId) {
        const url = `${SPORT_CENTER_API_BASE_URL}/${sportCenterId}/removeField/${fieldId}`;
        return apiClient.delete(url);
    }

}

export default new SportCenterService()