
import apiClient from "@/services/apiClient";

const RESERVATION_API_BASE_URL = 'http://localhost:8081/api/reservations'

class ReservationService{
    getAllReservations(){
        return apiClient.get(RESERVATION_API_BASE_URL)
    }

    checkAvailability(reservation){
        return apiClient.post(RESERVATION_API_BASE_URL + "/check-availability", reservation)
    }

    confirmReservation(reservation){
        return apiClient.post(RESERVATION_API_BASE_URL + "/confirm-reservation", reservation)
    }

}

export default new ReservationService()