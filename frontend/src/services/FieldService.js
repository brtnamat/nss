
import apiClient from "@/services/apiClient";

const FIELDS_API_BASE_URL = 'http://localhost:8081/api/fields'

class FieldService{
    getAllFields(){
        return apiClient.get(FIELDS_API_BASE_URL)
    }

    addField(newField){
        return apiClient.post(FIELDS_API_BASE_URL, newField)
    }

    getReservations(fieldId){
        const url = `${FIELDS_API_BASE_URL}/getReservations/${fieldId}`;

        return apiClient.get(url)
    }
}

export default new FieldService()