import { defineStore } from 'pinia';
import UserService from '@/services/UserService';
import {jwtDecode} from 'jwt-decode';

export const useAuthStore = defineStore('auth', {
    state: () => ({
        token: localStorage.getItem('token') || '',
        user: localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : null,
        status: '',
    }),
    getters: {
        isAuthenticated: (state) => !!state.token,
        authStatus: (state) => state.status,
        getUser: (state) => state.user,
    },
    actions: {
        async loginUser(user) {
            this.status = 'loading';
            try {
                const response = await UserService.generateJWTToken(user);
                console.log("Server response:", response);

                const token = response.data;
                const decoded = jwtDecode(token); // Decode the JWT token

                const userData = {
                    id: decoded.userId,
                    username: decoded.sub,  // Assuming "sub" is the username
                    role: decoded.authorities[0].authority,      // Assuming "role" is part of the token
                };
                this.setAuthData(token, userData);
                this.status = 'success';
            } catch (error) {
                this.status = 'error';
                this.clearAuthData();
                throw error;
            }
        },
        async logoutUser() {
            this.clearAuthData();
            this.status = '';
            return Promise.resolve();
        },
        setAuthData(token, userData) {
            localStorage.setItem('token', token);
            localStorage.setItem('user', JSON.stringify(userData));
            this.token = token;
            this.user = userData;
        },
        clearAuthData() {
            localStorage.removeItem('token');
            localStorage.removeItem('user');
            this.token = '';
            this.user = null;
        }
    }
});
