import { createRouter, createWebHistory } from 'vue-router';
import Home from '../views/HomePage.vue';
import Fields from '../views/SportCenterList.vue';
import FieldDetail from '../views/FieldDetail.vue';
import AddField from "../views/AddField.vue";
import SignUpPage from "@/views/SignUpPage.vue";
import SignInPage from "@/views/SignInPage.vue";
import SportCenterDetail from "@/views/SportCenterDetail.vue";
import PlayerDetail from "@/views/PlayerDetail.vue";
import OwnerDetail from "@/views/OwnerDetail.vue";
import {useAuthStore} from "@/store/auth";
import { storeToRefs } from 'pinia';

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home,
    },
    {
        path: '/sport-centers',
        name: 'Fields',
        component: Fields,
    },
    {
        path: '/fields/:id',
        name: 'FieldDetail',
        component: FieldDetail,
        props: true,
    },
    {
        path: '/fields/add-field',
        name: 'AddField',
        component: AddField
    },
    {
        path: '/sport-centers/:id',
        name: 'SportCenterDetail',
        component: SportCenterDetail,
        props: true
    },
    {
        path: "/sign-up",
        name: 'SignUp',
        component: SignUpPage
    },
    {
        path: "/sign-in",
        name: 'SignIn',
        component: SignInPage
    },
    {
        path: "/profile/player/:id",
        name: 'PlayerDetail',
        component: PlayerDetail,
        beforeEnter: (to, from, next) => {
            const authStore = useAuthStore();
            const { user, isAuthenticated } = storeToRefs(authStore);

            const userId = Number(to.params.id); // Convert userId to number
            const currentUserId = user.value?.id;

            if (isAuthenticated.value) {
                console.log('User is authenticated.');
                if (currentUserId === userId) {
                    console.log('User IDs match. Proceeding to route.'); // Debug output for successful authorization
                    next();
                } else {
                    console.log('User IDs do not match. Redirecting to home.'); // Debug output for failed authorization
                    next('/');
                }
            } else {
                console.log('User is not authenticated. Redirecting to home.'); // Debug output for failed authentication
                next('/');
            }
        }
    },
    {
        path: "/profile/owner/:id",
        name: 'OwnerDetail',
        component: OwnerDetail,
        props: true,
        beforeEnter: (to, from, next) => {
            const authStore = useAuthStore();
            const { user, isAuthenticated } = storeToRefs(authStore);

            const userId = Number(to.params.id); // Convert userId to number
            const currentUserId = user.value?.id;

            if (isAuthenticated.value) {
                console.log('User is authenticated.');
                if (currentUserId === userId) {
                    console.log('User IDs match. Proceeding to route.'); // Debug output for successful authorization
                    next();
                } else {
                    console.log('User IDs do not match. Redirecting to home.'); // Debug output for failed authorization
                    next('/');
                }
            } else {
                console.log('User is not authenticated. Redirecting to home.'); // Debug output for failed authentication
                next('/');
            }
        }
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;