import { createApp } from 'vue';
import PrimeVue from 'primevue/config';
import ToastService from "primevue/toastservice";
import Toast from "primevue/toast";
import 'primevue/resources/themes/aura-light-green/theme.css';
import 'primevue/resources/primevue.min.css';
import 'primeicons/primeicons.css';
import "primeflex/primeflex.css";
import { createPinia } from 'pinia';
import router from './router'

import App from './App.vue';

const app = createApp(App);
const pinia = createPinia();

// eslint-disable-next-line vue/multi-word-component-names
app.component('Toast', Toast);

app.use(ToastService);
app.use(pinia);
app.use(PrimeVue);
app.use(router);
app.mount('#app');